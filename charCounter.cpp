/* Algoritimo desenvolvido na aula de LP1 (linguagem de programação)
/  Autor: Arthur Cohen
/  Curso: Bacharel em Tenologia da Informacao - UFRN
/  04/03/2017
*/

#include <string>
#include "charCounter.h"
#include <iostream>

using namespace std;

void counting(int iPhrase[], int count[], int size){
    for(int i=0; i<size; i++){
        count[(iPhrase[i]-48)]++; //valor ASCII relacionado a posicao no array contador
    }
    for (int i = 0; i<74; i++){
        if (count[i]!=0) cout<<char(i+48)<<"="<<count[i]<<endl; //caso o valor ASCII i seja != 0, exibe frequencia
    }
}
