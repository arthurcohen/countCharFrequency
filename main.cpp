#include <iostream>
#include <cstring>
/* Algoritimo desenvolvido na aula de LP1 (linguagem de programação)
/  Autor: Arthur Cohen
/  Curso: Bacharel em Tenologia da Informacao - UFRN
/  04/03/2017
*/

#include <string>
#include "charCounter.h"

using namespace std;

int main(void){
    cout<<"digite uma frase:"<<endl;
    string phrase;
    getline(cin, phrase); //recupera frase do terminal
    int size = phrase.size()+1;

    char cPhrase[size]; //cria vetor de char da frase
    int iPhrase[size]; //cria vetor de int da frase 
    int count[74]; //vetor contador nas dimensoes da tabela ASCII
    for (int i=0; i<74; i++){
        count[i]=0; //garbage collector
    }
    strcpy(cPhrase, phrase.c_str()); //clone da string para array char

    for (int i=0; i<size; i++){
        iPhrase[i]=int(cPhrase[i]); //clone de char para array int (ASCII)
    }

    counting(iPhrase, count, size); //contagem de frequencia de caracteres numa string



    return 0;
}
